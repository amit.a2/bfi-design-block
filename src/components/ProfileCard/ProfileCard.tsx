// export  {ProfileCard} from "./ProfileCard/ProfileCard.tsx";import React from "react";
import styles from "./ProfileCard.module.css";
interface profilePropsState {
  name: string;
  imageUrl: string;
  nofprospect: string;
  totalnft: string;
}
const ProfileCard = ({
  name,
  imageUrl,
  nofprospect,
  totalnft,
}: profilePropsState) => {
  return (
    <div className={styles.card}>
        <div className={styles.card_person_detail}>
          <img className={styles.person_image} src={imageUrl} />
          <h3 data-testid="p_name" className={styles.person_name}>
            {name}
          </h3>
        </div>
        <div className={styles.info_wrapper}>
          <div className={styles.info_container}>
            <p className={styles.info_title}>Number of prospects</p>
            <p className={styles.info_value} data-testid="p_nop">
              {nofprospect}
            </p>
          </div>
          <div className={styles.info_container}>
            <p className={styles.info_title}>Total NFT Achieved</p>
            <p className={styles.info_value}>
              Rp <span data-testid="p_nft">{totalnft}</span>{" "}
            </p>
          </div>
        </div>
      <div className={styles.button_container}>
        <button
          className={styles.button_message}
          onClick={() => alert("Message Action Called")}
        >
          <img
            className={styles.button_icon}
            src="/assets/images/messageIcon.svg"
          />
          <p className={styles.button_text}>Message</p>
        </button>
        <button
          className={styles.button_call}
          onClick={() => alert("Call Action Called")}
        >
          <img
            className={styles.button_icon}
            src="/assets/images/callIcon.svg"
          />
          <p className={styles.button_text}>call</p>
        </button>
      </div>
    </div>
  );
};

export default ProfileCard;