import React from "react";
import logo from "./logo.svg";
import "./App.css";
import ProfileCard from "./components/ProfileCard/ProfileCard";

function App() {
  let props = {
    name: "Made Bayu",
    imageUrl: "/assets/images/personImage.svg",
    nofprospect: "23",
    totalnft: "80.500.00",
  };
  return (
    <>
      <ProfileCard {...props} />
    </>
  );
}

export default App;